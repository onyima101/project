# project

This creates a private registry using S3 bucket as the end storage.

LocalMachine-registry.yml only creates a private registry where data is saved on your local machine.

UI-frontend.yml only creates a UI Frontend for private registry. This works with either the registry that stores data on the local machine or/and s3 backend storage registry.

s3-registry.yml creates a docker registry using S3 as the backend storage for your data.

awskey-s3-registry-frontend.yml creates a docker registry (where AWS credentials are required) using S3 as the backend storage for your data alongside with a UI Frontend which can be accessed on http://YourDomainName:port

keyless-proxy-s3-registry-frontend.yml creates a docker UI as a proxy server to the docker registry. In this solution, AWS credentials are NOT required. The docker registry uses S3 as the backend storage for your data alongside with a UI Frontend which can be accessed on https://YourDomainName
